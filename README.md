A project of Arduino Uno Clone in KiCAD.

In the project there are two pcb layouts made.
In the arduino_uno_clone.kicad_pcb the layout is made in a four layer stackup,
whereas in arduino_uno_clone_2_layers.kicad_pcb the layout is made in a two
layer stackup.

The list of the components:
- ATMEGA328P-AU
- 3.3V Battery
- 330Ω 0805 resistor
- 10kΩ 0805 resistor
- Two 22pF 0805 capacitors
- 1µF 0805 polarized capacitor
- Two 24LC1025 I2C Serial EEPROM units
- DS1337S+ clock unit
- 0805 LED
- 32MHz 3.2x1.5mm crystal
- 16MHz 5.0x3.2mm crystal
- Two generic 01x04 connector
- generic 02x03 connector
- generic 01x09 connector
